package base;

import io.restassured.RestAssured;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

public class BaseTest {
    public static String resKey="AccessToken";
    public static String resValue="fb8af4ffe2ee5485d13a";
    public static String pathCheers="/v1/cheers";

    @BeforeSuite
    public void setupURL() {
        RestAssured.baseURI = "https://api.tinypulse.com";
        RestAssured.port = 443;
    }

    @BeforeClass
    public void beforeClass() {
        System.out.print("Start before class: \n");
    }

    @AfterClass
    public void afterClass() {
        System.out.print("After class: \n");
    }

    @AfterSuite
    public void afterSuite() {
        System.out.print("After suite: \n");
    }
}