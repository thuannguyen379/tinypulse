package cheers;

import base.BaseTest;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public class CheersTest extends BaseTest {
    //String path = "/v1/cheers";

    @Test(priority = 1, description = "request is success")
    public void TC01_WhenResIsSuccess() {
        given().header(resKey, resValue)
                .when()
                .request("GET", pathCheers)
                .then()
                .assertThat().statusCode(200)
                .body("data[0].id", Matchers.notNullValue())
                .body("data[0].type", Matchers.containsString("cheers"))
                .body("data[0].attributes.praise", Matchers.anything())
                .body("data[0].attributes.created_at", Matchers.anything())
                .body("data[0].attributes.sender_name", Matchers.anything())
                .body("data[0].attributes.sender_email", Matchers.anything())
                .body("data[0].attributes.receiver_name", Matchers.anything())
                .body("data[0].attributes.receiver_email", Matchers.anything())
                .body("data[0].attributes.sender_segment", Matchers.anything())
                .body("data[0].attributes.value_tags[0].tag", Matchers.anything())
                .body("data[0].attributes.value_tags[0].value", Matchers.anything())
                .body("data[0].attributes.value_tags[0].emoji", Matchers.anything())
                .body("data[0].relationships", Matchers.anything())
                .body("data[0].relationships.sender_segment", Matchers.anything())
                .body("data[0].relationships.sender_segment.data", Matchers.anything())
                .body("data[0].relationships.sender_segment.data.id", Matchers.anything())
                .body("data[0].relationships.sender_segment.data.type", Matchers.anything())
                .body("data[0].relationships.sender_segment.links", Matchers.anything())
                .body("data[0].relationships.sender_segment.links.related", Matchers.anything())
                .body("data[0].relationships.comments", Matchers.anything());
    }

    @Test(priority = 2, description = "Validate response time")
    public void TC02_WhenResponseTime() {
        Response response = RestAssured.get(pathCheers);
        long timeInMS = response.time();
        assertThat(timeInMS, lessThanOrEqualTo(3000L)); // response <= 3 seconds
    }

    @Test(priority = 3, description = "Wrong method")
    public void TC03_WhenResWrongMethod() {
        given().header(resKey, resValue)
                .when()
                .request("POST", pathCheers)
                .then()
                .assertThat().statusCode(405) //405 Method Not Allowed
                .body(Matchers.emptyOrNullString());
    }

    @Test(priority = 4, description = "Header wrong key")
    public void TC04_WhenResWrongKey() {
        given().header("x-api-key", resValue)
                .when()
                .request("GET", pathCheers)
                .then()
                .assertThat().statusCode(401)
                .body(Matchers.emptyString());
    }

    @Test(priority = 5, description = "Header wrong value")
    public void TC05_WhenResWrongValue() {
        given().header(resKey, "123123")
                .when()
                .request("GET", pathCheers)
                .then()
                .assertThat().statusCode(401)
                .body(Matchers.emptyString());
    }

    @Test(priority = 6, description = "Missing key")
    public void TC06_WhenResMisKey() {
        given().header("", resValue)
                .when()
                .request("GET", pathCheers)
                .then()
                .assertThat().statusCode(401)
                .body(Matchers.emptyString());
    }

    @Test(priority = 7, description = "Missing value")
    public void TC07_WhenResMisValue() {
        given().header(resKey, "")
                .when()
                .request("GET", pathCheers)
                .then()
                .assertThat().statusCode(401)
                .body(Matchers.emptyString());
    }

    @Test(priority = 8, description = "Header null key")
    public void TC08_WhenResNullKey() {
        given().header("null", resValue)
                .when()
                .request("GET", pathCheers)
                .then()
                .assertThat().statusCode(401)
                .body(Matchers.emptyOrNullString());
    }

    @Test(priority = 9, description = "Header null value")
    public void TC09_WhenResNullValue() {
        given().header(resKey, "null")
                .when()
                .request("GET", pathCheers)
                .then()
                .assertThat().statusCode(401)
                .body(Matchers.emptyOrNullString());
    }
}