import base.BaseTest;
import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class CommonTest extends BaseTest {
    //String path = "/v1/cheers";

    @Test(priority = 1, description = "Wrong URL")
    public void TC01_WhenResWrongURLCheers() {
        given().header(resKey, resValue)
                .when()
                .request("GET", pathCheers + "$")
                .then()
                .assertThat().statusCode(404);
    }
}
