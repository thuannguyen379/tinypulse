package pages;

import entilies.LoginEntilies;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import utilitys.ActionUtility;

import java.util.Date;

public class LoginPage {
    /**
     * Element Login
     */
    @FindBy(how = How.XPATH, using = "//input[@data-cucumber='input-login-email']")
    public WebElement emailOrUse;
    @FindBy(how = How.XPATH, using = "//input[@data-cucumber='input-login-password']")
    public WebElement pwd;
    @FindBy(how = How.XPATH, using = "//div[@data-cucumber='button-continue']//span[text()='Continue']")
    public WebElement continuebtn;
    @FindBy(how = How.XPATH, using = "//div[@data-cucumber='button-login']")
    public WebElement signInbtn;
    @FindBy(how = How.XPATH, using = "//figure[@data-cucumber='user-avatar']")
    public WebElement weAcount;
    @FindBy(how = How.XPATH, using = "//div[contains(text(),'Invalid email address')]")
    public WebElement errorLogin;


    private WebDriver driver;
    private ActionUtility waitU;


    public LoginPage(WebDriver driver) {
        this.driver = driver;
        initComponents();
    }

    private void initComponents() {
        PageFactory.initElements(driver, this);
        waitU = new ActionUtility(driver);
    }
    public void clearAll() {
        emailOrUse.clear();
        pwd.clear();
    }
    public void enterEmailOrUser(LoginEntilies loginEntilies) {
        emailOrUse.sendKeys(loginEntilies.getUser());
    }
    public void enterPwd(LoginEntilies loginEntilies) {
        pwd.sendKeys(loginEntilies.getPwd());
    }
    public void enterEmailWrong() {
        String unique = "something" + new Date().getTime() + "@gmail.com";
        emailOrUse.sendKeys(unique);
    }
    public void enterPassWrong() {
        String unique = "passwordisWrong";
        pwd.sendKeys(unique);
    }
    public void continueBtn() {
        waitU.waitUntilToBeClickAble(continuebtn);
        continuebtn.click();
    }
    public void SignInBtn() {
        waitU.waitUntilToBeClickAble(signInbtn);
        signInbtn.click();
    }
    /**
     * My element is displayed success
     */
    public boolean isMessageDisplayed(WebElement ele) {
        try {
            return ele.isDisplayed();
        } catch (NoSuchElementException ex) {
            return false;
        }
    }
}