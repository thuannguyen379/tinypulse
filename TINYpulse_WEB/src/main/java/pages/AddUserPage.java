package pages;

import entilies.AddUserEntilies;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import utilitys.ActionUtility;

import java.util.Date;

public class AddUserPage {
    @FindBy(how = How.XPATH, using = "//i[@class='icon-people-setting']")
    public WebElement iconSeting;
    @FindBy(how = How.XPATH, using = "//a[@href='/invite']//div[contains(text(),'Add People')]")
    public WebElement addPeopleBtn;
    @FindBy(how = How.XPATH, using = "//div[@class='mv3 f4 fw6']")
    public WebElement addEmpText;

    @FindBy(how = How.XPATH, using = "//input[@field='firstName'][@refkey='1']")
    public WebElement firstName;
    @FindBy(how = How.XPATH, using = "//input[@field='lastName'][@refkey='1']")
    public WebElement lastName;
    @FindBy(how = How.XPATH, using = "//input[@field='email'][@refkey='1']")
    public WebElement email;
    @FindBy(how = How.XPATH, using = "//span[@class='Button__text___13OYb'][contains(text(),'Add People')]")
    public WebElement addBtn;
    @FindBy(how = How.XPATH, using = "//a[@href='/invite'][contains(text(),'Back')]")
    public WebElement back;
    @FindBy(how = How.XPATH, using = "//a[@href='https://staging.tinyserver.info/engage']")
    public WebElement backToBoad;
    @FindBy(how = How.XPATH, using = "//div[contains(text(),'Congratulations')]")
    public WebElement congratuations;
    @FindBy(how = How.XPATH, using = "//div[contains(text(),'email already exists')]")
    public WebElement isError;
    @FindBy(how = How.XPATH, using = "//input[@field='firstName'][@refkey='1']/following-sibling::div")
    public WebElement isErrorBlankFis;
    @FindBy(how = How.XPATH, using = "//input[@field='email'][@refkey='1']/following-sibling::div")
    public WebElement isErrorBlankEmai;

    private WebDriver driver;
    private ActionUtility waitU;

    public AddUserPage(WebDriver driver) {
        this.driver = driver;
        initComponents();
    }

    private void initComponents() {
        PageFactory.initElements(driver, this);
        waitU = new ActionUtility(driver);
    }

    public void clearAll() {
        firstName.clear();
        lastName.clear();
        email.clear();
    }

    public void setingByIcon() {
        waitU.waitUntilToBeClickAble(iconSeting);
        iconSeting.click();
    }

    public void addPeopleMenu() {
        waitU.waitUntilToBeClickAble(addPeopleBtn);
        waitU.click(addPeopleBtn);

    }

    /**
     * My element is displayed success
     */

    public boolean isMessageDisplayed(WebElement ele) {
        try {
            return ele.isDisplayed();
        } catch (NoSuchElementException ex) {
            return false;
        }
    }

    public void fillFirst(AddUserEntilies addUserEntilies) {
        waitU.waitUntilToBeClickAble(firstName);
        firstName.sendKeys(addUserEntilies.getFirstName());
    }

    public void fillLast(AddUserEntilies addUserEntilies) {
        waitU.waitUntilToBeClickAble(lastName);
        lastName.sendKeys(addUserEntilies.getLastName());
    }

    public void fillEmail() {
        waitU.waitUntilToBeClickAble(email);
        String uniqueEmail = new Date().getTime() + "@gmail.com";
        email.sendKeys(uniqueEmail);
    }

    public void fillEmailExist(AddUserEntilies addUserEntilies) {
        waitU.waitUntilToBeClickAble(email);
        email.sendKeys(addUserEntilies.getEmail());
    }

    public void addPeopleBtn() {
        waitU.waitUntilToBeClickAble(addBtn);
        addBtn.click();
    }

    public void back() {
        waitU.waitUntilToBeClickAble(back);
        back.click();
    }

    public void backToAddPeople() {
        waitU.waitUntilToBeClickAble(backToBoad);
        backToBoad.click();
    }
}