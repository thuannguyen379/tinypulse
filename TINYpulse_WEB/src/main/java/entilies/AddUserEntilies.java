package entilies;

public class AddUserEntilies {
    private String firstName;
    private String lastName;
    private String email;

    private AddUserEntilies(Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.email = builder.email;
    }

    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getEmail() {
        return email;
    }

//    public List<String> getPeopleToList() {
//        List<String> supplierNames = new LinkedList<>();
//        supplierNames.add(firstName);
//        supplierNames.add(lastName);
//        supplierNames.add(email);
//        return  supplierNames;
//    }

    @Override
    public String toString() {
        return "{getFirst: " + firstName +
                "getLast" + lastName +
                "getEmail" + email +
                "}";
    }

    public static class Builder {
        private String firstName;
        private String lastName;
        private String email;

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

//        public Builder addpeople(String firstName,String lastName, String email) {
//            this.firstName = firstName;
//            this.lastName = lastName;
//            this.email = email;
//            return this;
//        }

        public AddUserEntilies build() {
            return new AddUserEntilies(this);
        }
    }
}
