package utilitys;

import entilies.AddUserEntilies;
import entilies.LoginEntilies;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

import java.io.File;

public class JsonUtility {
    JSONObject jsonObjectForm;

    public JsonUtility() {
        initComponents();
    }

    /**
     * Parse file JSON to data
     */
    public void initComponents() {
        try {
            this.jsonObjectForm = this.parseContent("\\data\\Dataset.json");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public JSONObject parseContent(String dir) throws Exception {
        File file = new File(System.getProperty("user.dir") + dir);
        String content = FileUtils.readFileToString(file, "utf-8");
        return new JSONObject(content);
    }

    public LoginEntilies emailLogin() {
        try {
            JSONObject localLogin = jsonObjectForm.getJSONObject("data");
            JSONObject localJsonObject = localLogin.getJSONObject("1st");
            return new LoginEntilies.Builder()
                    .user(localJsonObject.getString("use"))
                    .build();
        } catch (Exception e) {
        }
        return null;
    }

    public LoginEntilies pwd() {
        try {
            JSONObject localLogin = jsonObjectForm.getJSONObject("data");
            JSONObject localJsonObject = localLogin.getJSONObject("1st");
            return new LoginEntilies.Builder()
                    .pwd(localJsonObject.getString("pwd"))
                    .build();
        } catch (Exception e) {
        }
        return null;
    }

    public AddUserEntilies fisName() {
        try {
            JSONObject localLogin = jsonObjectForm.getJSONObject("data");
            JSONObject localJsonObject = localLogin.getJSONObject("1st");
            return new AddUserEntilies.Builder()
                    .firstName(localJsonObject.getString("fist"))
                    .build();
        } catch (Exception e) {
        }
        return null;
    }

    public AddUserEntilies lasName() {
        try {
            JSONObject localLogin = jsonObjectForm.getJSONObject("data");
            JSONObject localJsonObject = localLogin.getJSONObject("1st");
            return new AddUserEntilies.Builder()
                    .lastName(localJsonObject.getString("last"))
                    .build();
        } catch (Exception e) {
        }
        return null;
    }

    public AddUserEntilies email() {
        try {
            JSONObject localLogin = jsonObjectForm.getJSONObject("data");
            JSONObject localJsonObject = localLogin.getJSONObject("1st");
            return new AddUserEntilies.Builder()
                    .email(localJsonObject.getString("email"))
                    .build();
        } catch (Exception e) {
        }
        return null;
    }
}