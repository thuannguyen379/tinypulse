package utilitys;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ActionUtility {
    private static final int TIMEOUT_INTERVAL_UNIT = 30;
    private WebDriver driver;
    private WebDriverWait wait;
    private JavascriptExecutor js;

    public ActionUtility(WebDriver driver) {
        this.driver = driver;
        initComponents();
    }

    private void initComponents() {
        wait = new WebDriverWait(driver, TIMEOUT_INTERVAL_UNIT);
        js = ((JavascriptExecutor) driver);
    }

    public void click(WebElement click) {
        js.executeScript("arguments[0].click();", click);
    }

    public void waitUntilToBeClickAble(WebElement ele) {
        wait.until(ExpectedConditions.elementToBeClickable(ele));
    }

    public void waitUntilInVisibility(WebElement ele) {
        wait.until(ExpectedConditions.invisibilityOf(ele));
    }

    public void stopPageLoad() {
        js.executeScript("return window.stop");
    }
}
