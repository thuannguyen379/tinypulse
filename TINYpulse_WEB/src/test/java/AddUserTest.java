import org.testng.Assert;
import org.testng.annotations.Test;
import pages.AddUserPage;
import pages.LoginPage;
import setup.SetupBrowser;
import utilitys.JsonUtility;

public class AddUserTest extends SetupBrowser {
    LoginPage loginPage;
    AddUserPage addUser;
    JsonUtility jsonUtility;

    @Override
    public void preCondition() {
        loginPage = new LoginPage(driver);
        addUser = new AddUserPage(driver);
        jsonUtility = new JsonUtility();
    }

    @Test(description = "Click icon setting")
    public void TC01_clickIconSetting() {
        addUser.setingByIcon();
        Assert.assertTrue(addUser.isMessageDisplayed(addUser.addPeopleBtn));
    }

    @Test(description = "Click menu Add People")
    public void TC02_ClickAddPeopleMenu() {
        addUser.addPeopleMenu();
        Assert.assertTrue(addUser.isMessageDisplayed(addUser.addEmpText));
    }

    @Test(description = "Add user with email existing")
    public void TC03_AddUserEmailExist() {
        addUser.fillFirst(jsonUtility.fisName());
        addUser.fillLast(jsonUtility.lasName());
        addUser.fillEmailExist(jsonUtility.email());
        addUser.addPeopleBtn();
        Assert.assertTrue(addUser.isMessageDisplayed(addUser.isError));
        addUser.back();
    }

    @Test(description = "Add user with firstname is empty")
    public void TC04_AddUserFisEmpt() {
        addUser.clearAll();
        addUser.fillLast(jsonUtility.lasName());
        addUser.fillEmail();
        addUser.addPeopleBtn();
        Assert.assertTrue(addUser.isMessageDisplayed(addUser.isErrorBlankFis));
    }

    @Test(description = "Add user with email is empty")
    public void TC05_AddUserEmailEmpt() {
        addUser.clearAll();
        addUser.fillFirst(jsonUtility.fisName());
        addUser.fillLast(jsonUtility.lasName());
        addUser.addPeopleBtn();
        Assert.assertTrue(addUser.isMessageDisplayed(addUser.isErrorBlankEmai)); //Don't input email but added successul
    }

    @Test(description = "add new user successfully")
    public void TC06_AddUserIsSuccess() {
        addUser.backToAddPeople();
        addUser.setingByIcon();
        addUser.addPeopleMenu();
        addUser.clearAll();
        addUser.fillFirst(jsonUtility.fisName());
        addUser.fillLast(jsonUtility.lasName());
        addUser.fillEmail();
        addUser.addPeopleBtn();
        Assert.assertTrue(addUser.isMessageDisplayed(addUser.congratuations));
    }
}