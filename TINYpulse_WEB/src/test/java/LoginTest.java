import org.testng.Assert;
import org.testng.annotations.Test;
import pages.AddUserPage;
import pages.LoginPage;
import setup.SetupBrowser;
import utilitys.JsonUtility;

public class LoginTest extends SetupBrowser {
    LoginPage loginPage;
    AddUserPage addUser;
    JsonUtility jsonUtility;

    @Override
    public void preCondition() {
        loginPage = new LoginPage(driver);
        addUser = new AddUserPage(driver);
        jsonUtility = new JsonUtility();
    }

    @Test(priority = 1, description = "Login wrong Email")
    public void TC01_login_WrongEmail() {
        loginPage.enterEmailWrong();
        loginPage.continueBtn();
        loginPage.enterPwd(jsonUtility.pwd());
        loginPage.SignInBtn();
        Assert.assertTrue(loginPage.isMessageDisplayed(loginPage.errorLogin));
    }

    @Test(priority = 2, description = "Login wrong password")
    public void TC02_login_WrongPass() {
        loginPage.clearAll();
        loginPage.enterEmailOrUser(jsonUtility.emailLogin());
        loginPage.continueBtn();
        loginPage.enterPassWrong();
        loginPage.SignInBtn();
        Assert.assertTrue(loginPage.isMessageDisplayed(loginPage.errorLogin));
    }

    @Test(priority = 3, description = "Login successfully")
    public void TC03_login_Success() {
        loginPage.clearAll();
        loginPage.enterEmailOrUser(jsonUtility.emailLogin());
        loginPage.continueBtn();
        loginPage.enterPwd(jsonUtility.pwd());
        loginPage.SignInBtn();
        Assert.assertTrue(loginPage.isMessageDisplayed(loginPage.weAcount));
    }
}